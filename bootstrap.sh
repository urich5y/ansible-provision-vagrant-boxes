#!/bin/bash

CURRENT_USER=$(whoami)

HOSTS_CENTOS1="192.168.122.103 centos1"
HOSTS_CENTOS2="192.168.122.69 centos2"

# Make sure that the correct hosts are defined in /etc/hosts
verifyHosts() {
    if grep "${HOSTS_CENTOS1}" /etc/hosts >> /dev/null && grep "${HOSTS_CENTOS2}" /etc/hosts >> /dev/null; then
        echo "Hosts okay"
    else
        if grep "ˆ[0-9].*centos1" /etc/hosts >> /dev/null ; then
            echo "You appear to have hosts for the demo machines, but they do not match the project's IPs."
            echo "Please have the following in your /etc/hosts file and remove conflicting information:"
            echo "<------------"
            echo ${HOSTS_CENTOS1}
            echo "------------>"
        elif grep "ˆ[0-9].*centos2" /etc/hosts >> /dev/null ; then
                echo "You appear to have hosts for the demo machines, but they do not match the project's IPs."
                echo "Please have the following in your /etc/hosts file and remove conflicting information:"
                echo "<------------"
                echo ${HOSTS_CENTOS2}
                echo "------------>"
        else
            echo "Entering hosts data inside /etc/hosts... you may have to provide your password"
            echo "# centos1-centos2 <=> Formation Ansible" | sudo tee -a /etc/hosts >> /dev/null
            echo "${HOSTS_CENTOS1}" | sudo tee -a /etc/hosts >> /dev/null
            echo "${HOSTS_CENTOS2}" | sudo tee -a /etc/hosts >> /dev/null
        fi
    fi
}

verifyHosts
