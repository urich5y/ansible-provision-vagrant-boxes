## **Provisionnement de VM Vagrant avec Ansible**

**Avant-propos**

Ceci est un projet pour la création de 2 VMs avec VirtualBox en utilisant Vagrant et en réalisant le provisionnement avec Ansible.

Ce projet a été initié comme préalable à la formation **Ansible** organisée par Red Hat le 12 décembre 2017.

En effet, le formateur demandait de préparer 2 VMs avec notamment Ansible Tower sur l'une avant le début du cours.

Le "Vagrantfile" génère donc 2 VMs (centos1 et centos2) toutes 2 basées sur une box Centos 7.x.

L'installation prend environ 15 mn selon la vitesse de votre connexion internet.
 
**Prérequis**

Au moment de la rédaction de ce README, la version de Vagrant **(2.0.1)** ne prend pas en charge les version de Virtualbox 5.2.x.

Il faut donc revenir à Virtualbox 5.1.x **(5.1.30)**.

1) Lancer le script **bootstrap.sh** pour modifier le fichier /etc/hosts sur la machine hôte
```
./bootstrap.sh
```

2) Démarrer vagrant
```
vagrant up
```

3) A la fin de l'installation, Ansible Tower est installé
Pour se connecter à l'interface graphique, ignorer l'avertissement du navigateur web du fait du certificat SSL 'non sécuritaire' puis utiliser l'usager "admin" avec le mot de passe "1qaz2wsx"
```
https://centos1
```

Une fois connecté, il faut fournir un fichier contenant une clé d'activation.

Au moment de la formation, le fichier devait contenir les lignes suivantes:
```
{
"company_name": "Red Hat",
"contact_email": "mlessard@redhat.com",
"contact_name": "Michael Lessard",
"hostname": "37549211db3f44a0853a39d534aeca89",
"instance_count": 5,
"license_date": 1514729632,
"license_key": "daee39d1c86c4080090a867fd3b3156c9ba1dd4e2c07576d07bfedfe5ea9bfff", "license_type": "enterprise",
"subscription_name": "Red Hat Ansible Tower, Standard (5 Managed Nodes) Trial",
"trial": true 
}
```

3) Connexion SSH 
Pour se connecter par SSH en utilisant les clés générés par vagrant, utiliser les commandes suivantes:
```
vagrant ssh-config centos1 > vagrant_ssh_centos1
sed "s/User vagrant/User centos/" vagrant_ssh_centos1 > centos_ssh_centos1
ssh -F centos_ssh_centos1 centos1
```
Vous pouvez ensuite vous connecter sur centos2
```
[centos@centos1 ~]$ ssh centos2
``` 

4) Création/édition des fichiers Ansible
Vim est installé sur la VM centos1 mais vous pouvez utiliser votre éditeur préféré sur l'hôte en se placant sur le répertoire "centos1-share". Il y a un lien symbolique dans le HOME directory de centos qui pointe vers ce dossier 

## Références externes
http://www.hashbangcode.com/blog/connecting-vagrant-box-without-vagrant-ssh-command
